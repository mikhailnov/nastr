#!/bin/bash
# Thanks to Denis Linvinus <linvinus@gmail.com> for the base of this script

pkg_name="nastr"

# this allows the script to be ran both from the root of the source tree and from ./debian directory
dir_start="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ "$(basename "${dir_start}")" = 'debian' ]; then
	cd ..
fi
dir0="$(pwd)"
old_header=$(head -1 ./debian/changelog)

for i in xenial bionic focal groovy hirsute
do
	old_version="$(cat ./debian/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
	new_version="${old_version}~${i}1"
	sed -i -re "s/${old_version}/${new_version}/g" ./debian/changelog
	sed -i -re "1s/unstable/$i/" ./debian/changelog
	# -I to exclude .git; -d to allow building .changes file without build dependencies installed
	dpkg-buildpackage -I -S -sa -d
	sed  -i -re "1s/.*/${old_header}/" ./debian/changelog
	cd ..
	
	# change PPA names to yours, you may leave only one PPA; I upload nastr to 2 different PPAs at the same time
	for reponame in ppa:mikhailnov/utils #ppa:mikhailnov/desktop1-dev
	do
		if [ -f "${pkg_name}_${new_version}_source.changes" ]
			then dput -f "$reponame" "${pkg_name}_${new_version}_source.changes"
			else echo ".changes file ${pkg_name}_${new_version}_source.changes not found, not uploading anything!"
		fi
	done
	
	cd "$dir0"
	sleep 1
done

cd "$dir_start"
