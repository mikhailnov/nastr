#!/bin/bash
# License: GPLv3
# Author: mikhailnov

for conf_file in "/etc/nastr/10-nastr.conf" "./conf/10-nastr.conf"
do
	if [ -f "$conf_file" ]; then
		. "$conf_file"
	fi
done 

##################################################################################
case "$1" in
	"id")
		echo "${hw_inventory_id}"
	;;
	*)
		echo "\`hw-probe-nixtux id\` to print Inventory ID at https://linux-hardware.org"
		echo "Inventory ID is: ${hw_inventory_id}"
		hw-probe -all -upload -inventory-id "${hw_inventory_id}"
	;;
esac
