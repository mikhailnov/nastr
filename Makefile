#
PREFIX = /
TMPHOME=/tmp/wirec-build

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/usr/bin
	install -d $(DESTDIR)/$(PREFIX)/usr/sbin
	install -m0755 ./hw-probe.sh $(DESTDIR)/$(PREFIX)/usr/bin/hw-probe-nixtux
	install -m0755 ./oblako-remote.sh $(DESTDIR)/$(PREFIX)/usr/bin/oblako-remote
	install -m0755 ./nastr.sh $(DESTDIR)/$(PREFIX)/usr/sbin/nastr-setup
	
	install -d $(DESTDIR)/$(PREFIX)/usr/share/nastr/repolists
	# -t isn't recursive however - if srcdir contains directories, then they won't get copied
	install -m0644 ./repolists/* -t $(DESTDIR)/$(PREFIX)/usr/share/nastr/repolists/
	
	install -d $(DESTDIR)/$(PREFIX)/etc/nastr
	install -d $(DESTDIR)/$(PREFIX)/etc/nastr/torrc.d
	install -d $(DESTDIR)/$(PREFIX)/lib/systemd/system-preset
	# -t isn't recursive however - if srcdir contains directories, then they won't get copied
	install -m0644 ./conf/* -t $(DESTDIR)/$(PREFIX)/etc/nastr/
	install -m0644 ./tor/10-nastr-tor.conf $(DESTDIR)/$(PREFIX)/etc/nastr/torrc.d/10-nastr-tor.conf
	install -m0644 ./tor/20-nastr-tor-ssh.conf $(DESTDIR)/$(PREFIX)/etc/nastr/torrc.d/20-nastr-tor-ssh.conf
	install -m0644 ./tor/85-nastr-tor.preset $(DESTDIR)/$(PREFIX)/lib/systemd/system-preset/85-nastr-tor.preset
	
	install -d $(DESTDIR)/$(PREFIX)/etc/sysctl.d
	# -t isn't recursive however - if srcdir contains directories, then they won't get copied
	install -m0644 ./sysctl.conf $(DESTDIR)/$(PREFIX)/etc/sysctl.d/99-nixtux.conf
	
	install -d $(DESTDIR)/$(PREFIX)/usr/share/icons
	install -d $(DESTDIR)/$(PREFIX)/usr/share/applications
	install -m0644 ./desktop/oblako-remote.desktop $(DESTDIR)/$(PREFIX)/usr/share/applications/oblako-remote.desktop
	
	# nastr-mime
	install -d $(DESTDIR)/$(PREFIX)/etc/xdg/nastr
	install -d $(DESTDIR)/$(PREFIX)/etc/X11/Xsession.d/
	install -m0644 ./xdg-settings/mimeapps.list $(DESTDIR)/$(PREFIX)/etc/xdg/nastr/
	# nastr-xdg.sh must be sourced after /etc/X11/Xsession.d/60x11-common_xdg_path, because it initially sets XDG_CONFIG_DIRS, which is then modified by nastr-xdg.sh
	install -m0644 ./xdg-settings/nastr-xdg.sh $(DESTDIR)/$(PREFIX)/etc/X11/Xsession.d/61-nastr-xdg
	
	mkdir -p $(TMPHOME)
	env HOME=$(TMPHOME) libreoffice --headless --convert-to svg --outdir $(DESTDIR)/$(PREFIX)/usr/share/icons desktop/oblako-remote.odg

	. /etc/os-release ; \
	if [ "$$(echo $$VERSION_ID | sed -e 's,\.,,g')" -ge 2004 ]; \
	then \
		install -Dm0644 ./repolists/50-nastr-repos $(DESTDIR)/$(PREFIX)/etc/apt/preferences.d/50-nastr-repos ; \
	else \
		mkdir -p $(DESTDIR)/$(PREFIX)/etc/apt/preferences.d/ ; \
		echo > $(DESTDIR)/$(PREFIX)/etc/apt/preferences.d/50-nastr-repos ; \
	fi
	
uninstall:
	rm -fv $(PREFIX)/usr/bin/hw-probe-nixtux
	rm -fv $(PREFIX)/usr/bin/oblako-remote
	rm -fv $(PREFIX)/usr/sbin/nastr-setup
	rm -fv $(PREFIX)/usr/share/applications/oblako-remote.desktop
	rm -fv $(PREFIX)/usr/share/icons/oblako-remote.svg
	rm -frv $(PREFIX)/etc/nastr/
	rm -frv $(PREFIX)/etc/xdg/nastr/
	rm -fv $(PREFIX)/etc/X11/Xsession.d/61-nastr-xdg
	rm -frv $(PREFIX)/usr/share/nastr/

