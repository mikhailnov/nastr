# TODO: что скрипту осталось научиться делать
Потом в README.md перечислить его функции.

Знаком `+` помеченреализованные в nastr.sh пункты TODO

* Настраивать XFCE через консольные команды
* То, что консольно нельзя настроить, настраивать путем копирования файлов
* * Тема Minwaita (опакетить ее) (?)
* * Тема значков Vivacious-NonMono-Light-Blue (Runtu) (опакетить их) (или забить и использовать Elementary?)
* * PrintScreen - deepin-screenshot/flameshot (flameshot опакетить git для функции текста?)
* * Alt+PrintScreen - xfce4-screenshooter захват окна
* * Ctrl+PrintScreen - xfce4-screenshooter захват всего экрана
* * ScrollLock - Compose

* Geany как основной текстовый редактор:
* * Кодировка CP-1251 по умолчанию для файлов не в UTF-8
* * Документ -> Динамический перенос строк
* * Вертикальную полосу подсветки длинных строк убрать

* Audacious/DeaDBeef: CP-1251 по умолчанию
* deepin-screenshot/flameshot: сохранять скриншоты в папку ~/Изображения

* GTK+ 3 (~/.config/gtk-3.0/settings.ini):
* * gtk-primary-button-warps-slider=0 (https://developer.gnome.org/gtk3/stable/GtkSettings.html#GtkSettings--gtk-primary-button-warps-slider)
* * gtk-enable-animations=0 ("Whether to enable toolkit-wide animations")

* Мониторинг (Zabbix)

