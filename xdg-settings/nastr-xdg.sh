#!/bin/sh
# put to /etc/X11/Xsession.d/
#set -efu # script must not exit when being sourced, otherwise session does not start

# Be default on Xubuntu 18.04.1 it is:
# $ echo $XDG_CONFIG_DIRS
# /etc/xdg/xdg-xubuntu:/etc/xdg:/etc/xdg

# remove duplicates and make /etc/xdg go after /etc/xdg/nastr
# but /etc/xdg/xdg-xubuntu must be before nastr to allow nastr to override it
nastr='/etc/xdg/nastr'
# make sure that target directory exists
[ -d "$nastr" ] || return 1
# make sure that target variable to be modified is already set
[ -z "$XDG_CONFIG_DIRS" ] && return 1

orig="$(echo "$XDG_CONFIG_DIRS" | sed 's/\:/\n/g' | uniq)"
tail="$(echo "$orig" | tail -n1)"
# https://stackoverflow.com/a/25173311
new0="$(echo "$orig" | sed "\|^${tail}$|d")"
new1="$(echo "${new0}:${nastr}:${tail}" | tr '\n' ':' | sed -e 's/::/:/g' -e 's,:$,,g')"

# validate a bit and export if it seems to be correct
echo "$new1" | grep -q "${nastr}:${tail}" && \
! echo "$new1" | grep -q " " && \
[ "$(echo "$new1" | wc -l)" = 1 ] && \
export XDG_CONFIG_DIRS="$new1"

# new result is:
# XDG_CONFIG_DIRS=/etc/xdg/xdg-xubuntu:/etc/xdg/nastr:/etc/xdg
