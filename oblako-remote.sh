#!/bin/sh

create_dir(){
	if [ ! -d "$1" ]; then mkdir -p "$1"; fi
}

load_config(){
	# shellcheck source ./conf/10-nastr.conf
	for conf_file in "/etc/nastr/10-nastr.conf" "./conf/10-nastr.conf"
	do
		if [ -f "$conf_file" ]; then
			. "$conf_file"
		fi
	done
	
	create_dir "$conf_dir"
	if [ -f "$conf_oblako_remote" ]
		then
			. "$conf_oblako_remote"
		else
			echo "No config for oblako-remote in $conf_oblako_remote !"
			echo "See /etc/nastr/oblako-remote.conf for an example."
			echo "Using default config /etc/nastr/oblako-remote.conf for now."
			. "/etc/nastr/oblako-remote.conf"
	fi
}

umount_sshfs(){
	fusermount -u "${oblako_mount_dir}"
}

mount_sshfs(){
	create_dir "${oblako_mount_dir}"
	umount_sshfs || true
	
	if sshfs "${sshfs_username}@${sshfs_host}:/${sshfs_remote_dir}" "${oblako_mount_dir}" -p "${sshfs_port}"
		then
			xdg-open "${oblako_mount_dir}"
		else
			zenity --error --text "Ошибка подключения облака по SSHFS!" --width 350
	fi
}

load_config
case "$1" in
	mount )
		mount_sshfs 
	;;
	umount )
		umount_sshfs 
	;;
	* )
		echo "Usage: mount, umount"
	;;
esac
