#!/bin/bash
# Скрипт для автоматической настройки Xubuntu после установки. Используется в личный целях. Публично выложен для собственного удобства и т.к. кому-то может пригодиться.
# License: GPLv3
# Author: mikhailnov 

set -efux

for conf_file in "/etc/nastr/10-nastr.conf" "./conf/10-nastr.conf"
do
	if [ -f "$conf_file" ]; then
		. "$conf_file"
	fi
done

echo_err(){
	echo "$@" 1>&2
}

read_p(){
	# read -p is not compatible with POSIX shell
	echo -n "$@"
	read -r answer
	echo "$answer" >/dev/null
}

nastr_apt(){
	# based on https://superuser.com/a/725120
	# examples:
	# nastr_apt install "cups firefox ncdu htop"
	# nastr_apt "autoremove --purge" "cups firefox ncdu htop"
	pkg_list_input="$2"
	pkg_list_operation=""
	for pkgToProceed in $(echo $pkg_list_input); do
		if dpkg --status "$pkgToProceed" &> /dev/null; then
			pkgToRemoveList="$pkg_list_operation $pkgToProceed"
		fi
	done
	apt "$1" --yes $pkgToRemoveList
}

if [ "$(id -u)" != "0" ]; then
	echo_err "Скрипт нужно запустить от root!"
	echo_err "Run this script as root!"
	exit 1
fi

if [ "$BUILDING_ISO" != 1 ] && [ ! -f /etc/os-nixtux ]; then
	read_p "Enter system's uniq name (hostname):"
	echo "$answer" | tee /etc/os-nixtux
	# fstab will get into hw-probe
	echo "# os-nixtux: ${answer}" | tee -a /etc/fstab
	hostnamectl set-hostname "${answer}"
	echo "${answer}" | tee /etc/hostname
fi

arch_uname="$(uname -m)"
case "$arch_uname" in
	"x86_64")
		arch_pkg="amd64"
	;;
	"i386")
		arch_pkg="i386"
	;;
	* )
		echo "Unknown architecture!"
		exit 1
	;;
esac

. /etc/os-release
# 20.04 -> 2004
ubuntu_version="$(echo "$VERSION_ID" | sed -e 's,\.,,g')"
# check that is an integer and is not empty
if ! [ "$ubuntu_version" -ge 0 ]; then
	ubuntu_version=0
fi
if [ -f "repolists/${VERSION_CODENAME}.list" ]; then 
	cp -v "repolists/${VERSION_CODENAME}.list" /etc/apt/sources.list
fi

if ! grep -qr 'mikhailnov/utils' /etc/apt/sources.list.d/ ; then
	# '-n' may be not supported on Ubuntu<18.04
	add-apt-repository ppa:mikhailnov/utils -y -n || add-apt-repository ppa:mikhailnov/utils -y
fi
apt update

echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | debconf-set-selections
export DEBIAN_FRONTEND=noninteractive
	
# install all recommended depencies of the package 'nastr' which is used as a meta-package
apt install --reinstall --install-recommends -y nastr

# upgrades policykit-1 and all other packages
apt dist-upgrade -y

if [ "$ubuntu_version" -ge 1904 ]
	then kde_l10n_pkg=""
	else kde_l10n_pkg="kde-l10n-ru"
fi

# некоторые пакеты лишние, то есть являются зависимостями других пакетов из списка, но включены в список для удобства и наглядности, хотя apt пометит их как установленные вручную
apt install -y \
	language-pack-ru \
	language-pack-gnome-ru \
	libreoffice-l10n-ru \
	$kde_l10n_pkg \
	libreoffice \
	libreoffice-gtk3 \
	geany geany-plugins \
	wps-office \
	kdenlive \
	gimp krita krita-l10n kolourpaint \
	dumalogiya-fonts \
	ttf-mscorefonts-installer \
	nixtux-sysctl \
	gnome-disk-utility \
	hw-probe \
	deepin-screenshot scrot \
	gcolor3 \
	boomaga \
	kazam \
	keepassxc \
	ncdu \
	htop \
	mc \
	iotop \
	testdisk \
	ssh \
	xviewer \
	mpv \
	youtube-dl \
	system-autoupdate \
	telegram-desktop \
	guvcview \
	xarchiver \
	xfce4-goodies \
	arj rar unrar unar zstd squashfs-tools p7zip-full p7zip-rar unzip zip \
	qpdfview \
	okular okular-extra-backends kimageformat-plugins \
	chromium-browser chromium-codecs-ffmpeg-extra firefox firefox-locale-ru \
	synaptic \
	speedtest-cli \
	exfat-fuse exfat-utils \
	stress \
	sg3-utils \
	gparted \
	openssh-server \
	xpra \
	seahorse \
	screen \
	nfs-common

# nastr pulls xpra, we don't need xpra service, because we execute 'xpra shadow :0' manually via SSH
systemctl disable xpra || :

# language-pack-ru is not preinstalled in original ISO, we have installed it, now let's delete all other language packs
if [ "$BUILDING_ISO" = 1 ]; then
	language_packs_remove="$(dpkg -l | grep ^ii | awk '{print $2}' | grep ^language-pack | grep -Ev '\-ru|\-en')"
	[ -n "$language_packs_remove" ] && apt purge -y ${language_packs_remove}
fi

for i in \
	pidgin* \
	ristretto* \
	unattended-upgrades \
	update-notifier* \
	fonts-noto-cjk \
	kdeconnect \
	zfs* \
	zfs-zed \
	;
do
	apt autoremove --purge -y "$i" || :
done
# don't fail if these packages were already uninstalled when building ISO

apt dist-upgrade -y
apt autoremove -y

pushd /tmp
if ! dpkg -s teamviewer 1>/dev/null 2>/dev/null; then
	wget -O teamviewer.deb "https://download.teamviewer.com/download/linux/teamviewer_${arch_pkg}.deb"
	apt install -y ./teamviewer.deb && rm -fv ./teamviewer.deb
fi
popd
# I want TeamViewer to run out of the box in LiveCD
# But disable teamviewerd service in installed system
[ "$BUILDING_ISO" != 1 ] && dpkg -s teamviewer 1>/dev/null 2>/dev/null && teamviewer daemon disable

# match port in nastr-tor
sed -i -e 's,^#Port 22,Port 1909,g' /etc/ssh/sshd_config

###############################################################

# disable apport (отключить графические сообщения "Отправить отчет об ошибке?")
sed -i 's/enabled=1/enabled=0/g' /etc/default/apport

if [ "$BUILDING_ISO" != 1 ]; then
	# Disable Plymouth by removing "quite plash" and set automatic reboot on kernel panic
	# First comment out original line
	sed -i -e 's,^GRUB_TIMEOUT_STYLE=,#GRUB_TIMEOUT_STYLE=,g' /etc/default/grub
	sed -i -e 's,^GRUB_CMDLINE_LINUX_DEFAULT=,#GRUB_CMDLINE_LINUX_DEFAULT=,g' /etc/default/grub
	echo 'GRUB_CMDLINE_LINUX_DEFAULT="kernel.panic=30"' >> /etc/default/grub
	update-grub
fi

# Preinstall my public SSH keys
# It's a bad idea to preinstall them and to put them to /etc/skel, but I don't have any better ideas for now
# Preinstall my public SSH keys
# It's a bad idea to preinstall them and to put them to /etc/skel, but I don't have any better ideas for now
if [ -f conf/id_rsa_ISO_preinstalled.pub ]; then
	[ ! -d /etc/skel/.ssh ] && mkdir -p /etc/skel/.ssh
	# We need root access to LiveCDs
	[ ! -d /root/.ssh ] && mkdir -p /root/.ssh
	for i in /etc/skel/.ssh/authorized_keys /root/.ssh/authorized_keys
	do
		grep -q "$(cat conf/id_rsa_ISO_preinstalled.pub)" "$i" || \
				cat conf/id_rsa_ISO_preinstalled.pub >> "$i"
	done
	# User misha in installed system will have sudoers permissions,
	# SSH key will be authed in it because of copying from /etc/skel.
	# Let's delete key authorization for root if it exists
	if [ "$BUILDING_ISO" != 1 ]; then
			sed -i -e "/ $(head -n 1 conf/id_rsa_ISO_preinstalled.pub | awk '{print $2}' | head -c 5)/d" /root/.ssh/authorized_keys
	fi
fi
# Don't manipulate users and don't probe hardware if this script is ran when building ISO (in Cubic)
# Adding users to LiveCD breaks default login behaviour
[ "$BUILDING_ISO" = 1 ] && exit
# Rebuild initrd
update-initramfs -k "$(uname -r)" -u || :
adduser misha
adduser misha sudo
deluser user sudo
hw-probe-nixtux
